##  概要
---  

ggcp-logcenter 扩展是基础服务中「日志中心」业务日志数据记录服务的客户端组件。支持部分日志基本信息的校验及自动获取，同时 http 访问模式下底层服务调用依赖 mingyuanyun/ggcp-http 扩展，可自动采集相关服务调用日志信息


## 安装 && 配置  
---  

扩展的安装可以通过执行以下指令完成:  
```
composer require mingyuanyun/ggcp-logcenter

# 如果你的 Composer 版本是 2.x，最好加上相关执行参数
composer require --ignore-platform-reqs mingyuanyun/ggcp-logcenter
```  

扩展支持的所有配置:  

- `type` 日志中心服务调用模式，目前默认填 http 即可  
- `host` 日志中心的接口域名地址，比如 `https://www.domain.com`  
- `saveLoginLogUri` 保存登录日志的接口 URI  
- `saveAccessLogUri` 保存访问日志的接口 URI  
- `saveOperationLogUri` 保存数据变更日志的接口 URI  

配置示例:  

```php
$config = [
    'type' => 'http',
    'http' => [
        'host' => 'https://www.domain.com',
        'saveLoginLogUri' => '/save-login-log',
        'saveAccessLogUri' => '/save-access-log',
        'saveOperationLogUri' => '/save-operation-log',
    ],
];
```

实例化客户端示例:  

```php
$config = [...];
// 实例化客户端并注入配置
$client = \GgcpLogCenter\Client::instance($config);

// 先实例化客户端，需要时再注入配置
$client = \GgcpLogCenter\Client::instance();
...
$c = new \GgcpLogCenter\Config($config);
$client->setConfig($c);
```

## 记录日志
---  

日志中心一共支持三种类型日志的记录：登录日志、访问日志、操作日志。  

登录日志：用户在指定应用系统下的登录、登出行为日志记录。  
访问日志：用户在指定应用系统下对不同产品、菜单、功能模块的访问（点击）行为日志。  
操作日志：用户在指定应用系统下对不同产品的业务数据进行操作，并发生系统数据变化的行为日志。  

操作日志数据变化项类型分为三种：常规数据增删操作、数据属性项变更操作、数据关联项变更操作。  
常规数据增删操作日志：对某个业务数据进行添加、删除、批量导入等行为产生的日志。在这种操作行为下，一般我们只关注是哪个数据记录被动到即可。  
数据属性项变更操作日志：对某个业务数据进行了内容编辑、状态变更等行为产生的日志。在这种操作行为下，一般我们除了关注是哪个数据被动了以外，还需要关注该数据变更前后的信息内容对比。  
数据关联项变更操作日志：对某个业务数据的某种关联数据进行了添加、移除等关联/授权行为产生的日志。在这种操作行为下，一般我们关注是哪个数据的什么类型关联发生了变更。  


### 记录登录日志
```php
$config = [...];
$client = \GgcpLogCenter\Client::instance($config);

// 准备浏览器设备信息，内部逻辑自动获取 UserAgent 并解析
$device = \GgcpLogCenter\Logs\LogFactory::makeBrowserDevice([]);

$logValue = [
    'userId'     => 'xxx-xxx-xxx',   // 登录系统的用户 ID
    'tenantCode' => 'testTenant',    // 登录的租户号
    'system'     => 'composerTest',  // 登录进入的系统标识
    'ip'         => '127.0.0.1',     // 登录用户的客户端 IP 地址
    'device'     => $device,         // 登录用户的客户端设备信息，在这里是浏览器信息
    'action'     => LoginLog::LOGIN, // 登录还是登出动作
];
$loginLog = new \GgcpLogCenter\Logs\LoginLog($logValue);
// 保存日志
$client->save($loginLog);
```


### 记录访问日志
```php
$config = [...];
$client = \GgcpLogCenter\Client::instance($config);

// 准备浏览器设备信息，内部逻辑自动获取 UserAgent 并解析
$device = \GgcpLogCenter\Logs\LogFactory::makeBrowserDevice([]);

$logValue = [
    'userId'     => 'xxx-xxx-xxx',   // 登录系统的用户 ID
    'tenantCode' => 'testTenant',    // 登录的租户号
    'system'     => 'composerTest',  // 登录进入的系统标识
    'ip'         => '127.0.0.1',     // 登录用户的客户端 IP 地址
    'device'     => $device,         // 登录用户的客户端设备信息，在这里是浏览器信息
    'appCode'    => 'BasicService',  // 日志中心能识别的产品编码
    'accessPath' => '/产品A/菜单xx/功能点xxx', // 访问的功能在系统中的路径地址
];
$accessLog = new \GgcpLogCenter\Logs\AccessLog($logValue);
// 保存日志
$client->save($accessLog);
```


### 记录操作日志
```php
$config = [...];
$client = \GgcpLogCenter\Client::instance($config);

// 准备浏览器设备信息，内部逻辑自动获取 UserAgent 并解析
$device = \GgcpLogCenter\Logs\LogFactory::makeBrowserDevice([]);

// 常规数据增删操作日志数据格式
$logValue = [
    'userId'       => 'xxx-xxx-xxx',   // 登录系统的用户 ID
    'tenantCode'   => 'testTenant',    // 登录的租户号
    'system'       => 'composerTest',  // 登录进入的系统标识
    'ip'           => '127.0.0.1',     // 登录用户的客户端 IP 地址
    'device'       => $device,         // 登录用户的客户端设备信息，在这里是浏览器信息
    'appCode'      => 'BasicService',  // 日志中心能识别的产品编码
    'event'        => 'CREATE_USER',   // 操作事件
    'operateItems' => [                // 操作的数据项数组
        [
            'objectName'   => 'USER_01', // 操作的数据项数据名称（比如用户的话可以是用户名或者账号）
        ],
        [
            'objectName'   => 'USER_02',
        ],
    ],
];
// 数据属性项变更操作日志数据格式
$logValue = [
    'userId'       => 'xxx-xxx-xxx',   // 登录系统的用户 ID
    'tenantCode'   => 'testTenant',    // 登录的租户号
    'system'       => 'composerTest',  // 登录进入的系统标识
    'ip'           => '127.0.0.1',     // 登录用户的客户端 IP 地址
    'device'       => $device,         // 登录用户的客户端设备信息，在这里是浏览器信息
    'appCode'      => 'BasicService',  // 日志中心能识别的产品编码
    'event'        => 'CREATE_USER',   // 操作事件
    'operateItems' => [                // 操作的数据项数组
        [
            'objectName'   => 'USER_01', // 操作的数据项数据名称（比如用户的话可以是用户名或者账号）
            'changeValues' => [          // 数据项被操作发生内容变更的变更项数组
                ['action' => 'add', 'name' => '测试功能点01'],
                ['action' => 'add', 'name' => '测试功能点02'],
            ],
        ]
    ],
];
// 数据关联项变更操作日志数据格式
$logValue = [
    'userId'       => 'xxx-xxx-xxx',   // 登录系统的用户 ID
    'tenantCode'   => 'testTenant',    // 登录的租户号
    'system'       => 'composerTest',  // 登录进入的系统标识
    'ip'           => '127.0.0.1',     // 登录用户的客户端 IP 地址
    'device'       => $device,         // 登录用户的客户端设备信息，在这里是浏览器信息
    'appCode'      => 'BasicService',  // 日志中心能识别的产品编码
    'event'        => 'CREATE_USER',   // 操作事件
    'operateItems' => [                // 操作的数据项数组
        [
            'objectName'   => 'USER_01', // 操作的数据项数据名称（比如用户的话可以是用户名或者账号）
            'changeValues' => [          // 数据项被操作发生内容变更的变更项数组
                ['name' => '属性a', 'value' => 'a01', 'beforeValue' => 'a02'],
                ['name' => '属性b', 'value' => 'b01', 'beforeValue' => 'b02'],
            ],
        ]
    ],
];

$operationLog = new \GgcpLogCenter\Logs\LoginLog($logValue);
// 保存日志
$client->save($operationLog);
```