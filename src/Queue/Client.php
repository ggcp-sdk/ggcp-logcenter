<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日15:42:20
 */
namespace GgcpLogCenter\Queue;

use GgcpLogCenter\Logs\LogInterface;
use GgcpLogCenter\Result;
use GgcpLogCenter\Support\Traits\SingletonTrait;

/**
 * @method static $this instance(Config|array $config)
 */
class Client
{
    use SingletonTrait;

    /**
     * 队列客户端配置实例
     *
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param  LogInterface $log 需要记录的日志数据对象实现
     * @return Result 日志处理结果对象实例
     */
    public function push(LogInterface $log)
    {
        return Result::make(false, '暂不支持队列消费模式');
    }
}
