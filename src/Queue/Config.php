<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日15:43:24
 */
namespace GgcpLogCenter\Queue;

use GgcpLogCenter\Support\Traits\LoadPropsTrait;

class Config
{
    use LoadPropsTrait;

    /**
     * 队列类型
     */
    public $type;

    public function __construct(array $values)
    {
        $this->loadProps($values);
    }
}
