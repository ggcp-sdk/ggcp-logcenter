<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support;

class RequestDataHelper
{
    /**
     * 获取当前请求访问的外网 IP 地址
     *
     * @return string
     */
    public static function getWanIp()
    {
        if (!empty($_SERVER['HTTP_REMOTE_HOST'])) {
            return $_SERVER['HTTP_REMOTE_HOST'];
        }

        if (class_exists('\\Yii') && \Yii::$app->request instanceof \yii\web\Request) {
            return \Yii::$app->request->getUserIP();
        }
        return '127.0.0.1';
    }

    /**
     * 获取当前请求是由内网哪台机器 IP 转发过来的
     *
     * @return string
     */
    public static function getLanIp()
    {
        // IP 相关数据加载
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipArr = explode(',', str_replace(' ', '', $_SERVER['HTTP_X_FORWARDED_FOR']));
            if (count($ipArr) >= 2) {
                return $ipArr[1];
            }
        }
        return '';
    }

    /**
     * 获取当前访问请求 Header 中的 User-Agent
     *
     * @return string
     */
    public static function getUserAgent()
    {
        return !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }

    /**
     * 获取当前请求访问的链接地址
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        //加入判断是否是https请求
        $protocol = "http://";
        if (
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
        ) {
            $protocol = "https://";
        }
        return !empty($_SERVER['HTTP_HOST']) ? "$protocol{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" : '';
    }

    /**
     * 获取当前请求发起的来源地址
     *
     * @return string
     */
    public static function getOriginUrl()
    {
        return !empty($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '';
    }

    /**
     * 获取当前请求发起地址的前一个地址
     *
     * @return string
     */
    public static function getRefererUrl()
    {
        return !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
    }
}
