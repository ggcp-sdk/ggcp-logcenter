<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support;

use Yii;
use yii\log\Logger;

class YiiLogger
{
    public static function info($message, $category = 'application')
    {
        Yii::getLogger()->log($message, Logger::LEVEL_INFO, $category);
    }

    public static function warning($message, $category = 'application')
    {
        Yii::getLogger()->log($message, Logger::LEVEL_WARNING, $category);
    }

    public static function error($message, $category = 'application')
    {
        Yii::getLogger()->log($message, Logger::LEVEL_ERROR, $category);
    }
}
