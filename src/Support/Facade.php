<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support;

/**
 * 门面（外观）模式抽象类
 */
abstract class Facade
{
    /**
     * 门面处理器实例容器
     *
     * @var array
     */
    protected static $facadeInstances = [];

    /**
     * 获取注册的门面处理器
     *
     * @return string|object
     */
    protected static function getFacadeAccessor()
    {
        throw new \RuntimeException('Facade does not implement getFacadeAccessor method.');
    }

    /**
     * 获取门面实例
     *
     * @return mixed
     */
    protected static function getFacadeInstance()
    {
        return static::resolveFacadeInstance(static::getFacadeAccessor());
    }

    /**
     * 根据处理规则解决并最终获取一个可用的门面实例返回
     *
     * @param  mixed $name
     * @return mixed
     */
    protected static function resolveFacadeInstance($name)
    {
        if (is_object($name)) {
            return $name;
        }

        if (isset(static::$facadeInstances[$name])) {
            return static::$facadeInstances[$name];
        }

        if (is_string($name)) {
            static::$facadeInstances[$name] = new $name();
        } else {
            return null;
        }

        return static::$facadeInstances[$name];
    }

    /**
     * 动态地将静态方法调用转移到设置好的处理类
     *
     * @param  string $method   被调用的静态方法名
     * @param  array  $args     提供给被调用静态方法的参数数组
     * @return mixed
     * @throws \RuntimeException
     */
    public static function __callStatic($method, $args)
    {
        $instance = static::getFacadeInstance();
        if (!$instance) {
            throw new \RuntimeException('The facade accessor has not been set.');
        }

        return $instance->{$method}(...$args);
    }
}
