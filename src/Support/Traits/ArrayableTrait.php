<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support\Traits;

trait ArrayableTrait
{
    /**
     * 将类中的属性变量转成数组数据
     *
     * @return array
     */
    public function toArray()
    {
        $result = [];

        $fields = $this->fields();
        foreach ($fields as $field) {
            if (property_exists($this, $field)) {
                $result[$field] = $this->{$field};
            }
        }

        return $result;
    }

    /**
     * 支持数组操作的对象属性数组。只有被支持的属性才能在 toArray() 方法中被带上
     *
     * @return array
     */
    public function fields()
    {
        return array_keys(get_object_vars($this));
    }

    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetGet($offset)
    {
        return property_exists($this, $offset) ? $this->{$offset} : null;
    }

    public function offsetSet($offset, $value)
    {
        if (property_exists($this, $offset)) {
            $this->{$offset} = $value;
        }
    }

    public function offsetUnset($offset)
    {
        if (property_exists($this, $offset)) {
            $this->{$offset} = null;
        }
    }
}
