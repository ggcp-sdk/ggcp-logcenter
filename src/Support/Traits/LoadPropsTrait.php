<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support\Traits;

trait LoadPropsTrait
{
    /**
     * 将数组加载到对应的类属性中
     *
     * @param  array $props 属性数据数组
     */
    public function loadProps(array $props)
    {
        foreach ($props as $field => $value) {
            if (property_exists($this, $field)) {
                $this->{$field} = is_numeric($value) ? (int) $value : $value;
            }
        }
    }
}
