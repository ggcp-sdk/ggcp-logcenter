<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support\Traits;

use ReflectionClass;

/**
 * 类常量特性类插件，可以用于针对类常量的一些处理
 */
trait ConstantTrait
{
    /**
     * 校验一个常量是否正确（存在）
     *
     * @param  mixed  $constant     常量值
     * @param  string $fieldPrefix  需要过滤的常量名称的前缀，仅符合前缀的常量才进行判断
     * @return bool
     */
    public static function checkConstant($constant, $fieldPrefix = '')
    {
        $reflection = new ReflectionClass(static::class);

        $constants = [];
        if (empty($fieldPrefix)) {
            $constants = $reflection->getConstants();
        } else {
            foreach ($reflection->getConstants() as $field => $v) {
                if (strpos($field, $fieldPrefix) === 0) {
                    $constants[] = $v;
                }
            }
        }
        return in_array($constant, $constants);
    }

    /**
     * 校验类型是否正确
     *
     * @param  string $type 类型
     * @return bool
     */
    public static function checkType($type)
    {
        return static::checkConstant($type, 'TYPE_');
    }

    /**
     * 校验数据状态是否正确
     *
     * @param  mixed $status 数据状态
     * @return bool
     */
    public static function checkStatus($status)
    {
        return static::checkConstant($status, 'STATUS_');
    }

    /**
     * 校验事件类型是否正确
     *
     * @param  string $event 事件类型
     * @return bool
     */
    public static function checkEvent($event)
    {
        return static::checkConstant($event, 'EVENT_');
    }
}
