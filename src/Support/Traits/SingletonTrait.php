<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support\Traits;

/**
 * 单例模式特性组件
 */
trait SingletonTrait
{
    /**
     * 单例实例
     *
     * @var array
     */
    protected static $instances;

    /**
     * 获取一个实例单例类
     *
     * @return $this
     */
    public static function instance(...$args)
    {
        $className = static::class;
        if (!isset(static::$instances[$className]) || is_null(static::$instances[$className])) {
            static::$instances[$className] = new $className(...$args);
        }
        return static::$instances[$className];
    }
}
