<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support\Facades;

use GgcpLogCenter\Config;
use GgcpLogCenter\Support\Facade;

/**
 * @method static void info(string $message, string $category = 'application')
 * @method static void warning(string $message, string $category = 'application')
 * @method static void error(string $message, string $category = 'application')
 */
class Log extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Config::instance()->logger;
    }
}
