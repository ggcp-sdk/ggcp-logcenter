<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Support;

class EmptyLogger
{
    public static function info($message, $category = 'application')
    {
        echo $message . PHP_EOL;
    }

    public static function warning($message, $category = 'application')
    {
        echo $message . PHP_EOL;
    }

    public static function error($message, $category = 'application')
    {
        echo $message . PHP_EOL;
    }
}