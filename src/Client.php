<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日15:30:51
 */
namespace GgcpLogCenter;

use GgcpLogCenter\Http\Client as HttpClient;
use GgcpLogCenter\Logs\LogInterface;
use GgcpLogCenter\Queue\Client as QueueClient;
use GgcpLogCenter\Support\Traits\SingletonTrait;

/**
 * 公共产品服务 - 日志中心客户端
 * 日志中心客户端负责向业务方提供向日志中心服务模块中记录自己的业务日志数据
 * 并且记录成功后，在相应的日志中心模块下能完成对日志数据的检索、查看
 *
 * @method static $this instance()
 */
class Client
{
    // 单例实现
    use SingletonTrait;

    /**
     * 日志中心配置实例
     *
     * @var Config
     */
    private $config;

    /**
     * 构造一个日志中心客户端实例
     */
    public function __construct($config = [])
    {
        if (!empty($config)) {
            if ($config instanceof Config) {
                $this->config = $config;
            } elseif (is_array($config)) {
                $this->config = new Config($config);
            }
        }
    }

    /**
     * 设置日志中心的配置实例
     *
     * @param  Config $config 日志中心配置实例对象
     * @return $this
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * 切换为直接入列日志队列的模式来记录日志
     *
     * @return QueueClient
     */
    public function asQueueClient()
    {
        $queueConfig = $this->config->getQueueConfig();
        return QueueClient::instance($queueConfig);
    }

    /**
     * 切换为 Http 接口请求的模式来记录日志
     *
     * @return HttpClient
     */
    public function asHttpClient()
    {
        $httpConfig = $this->config->getHttpConfig();
        return HttpClient::instance($httpConfig);
    }

    /**
     * 保存日志
     *
     * @param  LogInterface $log 满足接口声明的日志数据实例
     * @return Result
     */
    public function save(LogInterface $log)
    {
        if (!isset($this->config)) {
            return Result::make(false, '未提供日志中心配置，无法使用日志中心服务');
        }

        if ($this->config->isUseHttp()) {
            return $this->asHttpClient()->invoke($log);

        } elseif ($this->config->isUseQueue()) {
            return $this->asQueueClient()->push($log);

        } else {
            return Result::make(false, '日志中心配置异常，请检查');
        }
    }
}
