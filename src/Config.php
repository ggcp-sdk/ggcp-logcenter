<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter;

use GgcpLogCenter\Exceptions\LogCenterConfigException;
use GgcpLogCenter\Http\Config as HttpConfig;
use GgcpLogCenter\Queue\Config as QueueConfig;
use GgcpLogCenter\Support\EmptyLogger;
use GgcpLogCenter\Support\Traits\LoadPropsTrait;
use GgcpLogCenter\Support\YiiLogger;

/**
 * @method static $this instance($config = [])
 */
class Config
{
    use LoadPropsTrait;

    /**
     * 当前配置使用的客户端模式类型: http|queue
     * 
     * @var string
     */
    public $type;
    
    /**
     * Http 接口请求模式下的配置对象实例
     *
     * @var HttpConfig
     */
    public $httpConfig;

    /**
     * 队列消费模式下的配置对象实例
     *
     * @var QueueConfig
     */
    public $queueConfig;

    /**
     * 系统日志处理器。默认使用 Yii 框架自带的日志处理器记录执行日志
     *
     * @var mixed
     */
    public $logger;

    /**
     * 记录日志的时区设置，默认使用上海的时区
     *
     * @var string
     */
    public $timezone = 'Asia/Shanghai';

    /**
     * 构造
     */
    public function __construct($config = [])
    {
        if (empty($config)) {
            throw new LogCenterConfigException('找不到 GgcpLogCenter 的配置数据');
        }
        if ($config['type'] === 'http' && empty($config['http'])) {
            throw new LogCenterConfigException('Http 接口调用模式下必须提供 http 的配置内容');
        }
        if ($config['type'] === 'queue' && empty($config['queue'])) {
            throw new LogCenterConfigException('队列消费模式下必须提供 queue 的配置内容');
        }

        if (!empty($config['http'])) {
            $config['httpConfig'] = new HttpConfig($config['http']);
        }
        if (!empty($config['queue'])) {
            $config['queueConfig'] = new QueueConfig($config['queue']);
        }

        $this->loadProps($config);

        if (is_null($this->logger)) {
            // 默认使用 Yii 框架的日志处理器
            $this->logger = class_exists('\\Yii') ? YiiLogger::class : EmptyLogger::class;
        }
    }

    /**
     * 获取 HTTP 接口模式下日志中心客户端所需的配置实例
     *
     * @return \GgcpLogCenter\Http\Config
     */
    public function getHttpConfig()
    {
        return $this->httpConfig;
    }

    /**
     * 获取队列模式下日志中心客户端所需的配置实例
     *
     * @return \GgcpLogCenter\Queue\Config
     */
    public function getQueueConfig()
    {
        return $this->queueConfig;
    }

    /**
     * 判断当前是否使用 Http 接口调用模式
     * 
     * @return bool
     */
    public function isUseHttp()
    {
        if (!$this->hasHttpConfig()) {
            return false;
        }
        return strtolower($this->type) === 'http';
    }

    /**
     * 判断当前是否使用队列消费模式
     * 
     * @return bool
     */
    public function isUseQueue()
    {
        if (!$this->hasQueueConfig()) {
            return false;
        }
        return strtolower($this->type) === 'queue';
    }

    /**
     * 判断当前是否有可用的 Http 模式配置
     *
     * @return bool
     */
    public function hasHttpConfig()
    {
        return isset($this->httpConfig);
    }

    /**
     * 判断当前是否有可用的队列消费模式配置
     *
     * @return bool
     */
    public function hasQueueConfig()
    {
        return isset($this->queueConfig);
    }
}
