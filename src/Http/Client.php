<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日15:59:34
 */
namespace GgcpLogCenter\Http;

use GgcpHttp\Response;
use GgcpHttp\Sender;
use GgcpLogCenter\Exceptions\HttpClientException;
use GgcpLogCenter\Exceptions\InvalidLogException;
use GgcpLogCenter\Logs\AccessLog;
use GgcpLogCenter\Logs\LoginLog;
use GgcpLogCenter\Logs\LogInterface;
use GgcpLogCenter\Logs\OperationLog;
use GgcpLogCenter\Result;
use GgcpLogCenter\Support\Traits\SingletonTrait;

class Client
{
    use SingletonTrait;

    /**
     * 日志中心服务调用配置实例
     *
     * @var Config
     */
    private $config;

    /**
     * 接口请求发送器
     *
     * @var Sender
     */
    private $sender;

    /**
     * 构造一个日志中心 Http 调用模式客户端实例
     *
     * @param  Config $config 发起日志中心 Http 调用所需的配置实例
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        if (empty($this->config->host)) {
            throw new HttpClientException('无效的日志中心服务接口请求域名');
        }

        $options = [
            'base_uri'        => $this->config->host,
            'connect_timeout' => $this->config->connectTimeout,
            'timeout'         => $this->config->timeout,
        ];
        $this->sender = \GgcpHttp\Client::prepare($options)->getSender();
    }

    /**
     * 统一的日志数据保存接口调用入口
     *
     * @param  LogInterface $log 继承该父类的日志对象子类实例
     * @return Result
     * @throws InvalidLogException 遇到不支持的子类实现时，会抛出该异常
     */
    public function invoke(LogInterface $log)
    {
        if ($log instanceof LoginLog) {
            return $this->invokeSaveLoginLog($log);

        } elseif ($log instanceof AccessLog) {
            return $this->invokeSaveAccessLog($log);

        } elseif ($log instanceof OperationLog) {
            return $this->invokeSaveOperationLog($log);

        } else {
            throw new InvalidLogException('无效的日志类型');
        }
    }

    /**
     * 调用登录日志保存接口
     *
     * @param LoginLog $log 登录日志数据对象
     */
    protected function invokeSaveLoginLog(LoginLog $log)
    {
        if (empty($this->config->saveLoginLogUri)) {
            throw new HttpClientException('无效的登录日志接口路由');
        }

        $uri      = $this->config->saveLoginLogUri;
        $data     = $log->toArray();
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用访问日志保存接口
     *
     * @param AccessLog $log 访问日志数据对象
     */
    protected function invokeSaveAccessLog(AccessLog $log)
    {
        if (empty($this->config->saveAccessLogUri)) {
            throw new HttpClientException('无效的访问日志接口路由');
        }

        $uri      = $this->config->saveAccessLogUri;
        $data     = $log->toArray();
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * 调用操作日志保存接口
     *
     * @param  OperationLog $log 操作日志数据对象
     */
    protected function invokeSaveOperationLog(OperationLog $log)
    {
        if (empty($this->config->saveOperationLogUri)) {
            throw new HttpClientException('无效的操作日志接口路由');
        }

        $uri      = $this->config->saveOperationLogUri;
        $data     = $log->toArray();
        $response = $this->sender->postWithJson($uri, $data);
        return $this->handleResponse($response);
    }

    /**
     * @param  Response $response 接口请求响应对象
     * @return Result
     */
    protected function handleResponse(Response $response)
    {
        $result = Result::make(false, '');
        if ($response->getStatusCode() !== 200) {
            $result->errmsg = $response->toString();
        } else {
            if (!$response->isSuccess(true)) {
                $data           = $response->toArray();
                $result->errmsg = $data['message'];
            } else {
                $result->ok = true;
            }
        }
        return $result;
    }
}
