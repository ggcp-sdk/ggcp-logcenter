<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日17:29:57
 */
namespace GgcpLogCenter\Http;

use GgcpLogCenter\Support\Traits\LoadPropsTrait;

class Config
{
    use LoadPropsTrait;
    
    /**
     * 日志中心服务地址链接（接口域名地址）
     *
     * @var string
     */
    public $host;

    /**
     * TCP 连接等待超时时间，默认 0.5s
     * 
     * @var int
     */
    public $connectTimeout = 0.5;

    /**
     * 接口等待超时时间，默认 1s
     * 
     * @var int
     */
    public $timeout = 1;

    /**
     * 登录日志服务地址 URI
     * 
     * @var string
     */
    public $saveLoginLogUri;

    /**
     * 访问日志服务地址 URI
     * 
     * @var string
     */
    public $saveAccessLogUri;

    /**
     * 操作日志服务地址 URI
     * 
     * @var string
     */
    public $saveOperationLogUri;

    public function __construct(array $values)
    {
        $this->loadProps($values);
    }
}