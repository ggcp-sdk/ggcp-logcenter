<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Exceptions;

use InvalidArgumentException;

class LogPropsException extends InvalidArgumentException
{
    
}