<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月06日15:25:26
 */
namespace GgcpLogCenter\Exceptions;

use RuntimeException;

class InvalidLogException extends RuntimeException
{
    
}