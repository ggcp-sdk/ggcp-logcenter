<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月06日16:05:50
 */
namespace GgcpLogCenter;

use GgcpLogCenter\Support\Traits\LoadPropsTrait;

class Result
{
    use LoadPropsTrait;

    /**
     * 日志记录是否成功
     *
     * @var bool
     */
    public $ok;

    /**
     * 失败时的错误描述内容
     *
     * @var string
     */
    public $errmsg;

    public function __construct(array $values)
    {
        $this->loadProps($values);
    }

    /**
     * 根据提供的内容构建一个日志处理结果对象实例
     *
     * @param  bool   $ok       日志处理是否成功，true 成功、false 失败
     * @param  string $errmsg   日志处理失败时的错误描述内容
     * @return $this
     */
    public static function make($ok, $errmsg = '')
    {
        return new static(['ok' => $ok, 'errmsg' => $errmsg]);
    }
}
