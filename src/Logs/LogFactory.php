<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月06日10:25:33
 */
namespace GgcpLogCenter\Logs;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Logs\Device\AppDevice;
use GgcpLogCenter\Logs\Device\BrowserDevice;
use GgcpLogCenter\Logs\Device\UnknownDevice;
use GgcpLogCenter\Logs\Operation\PropsChangeValue;
use GgcpLogCenter\Logs\Operation\RelationChangeValue;
use GgcpLogCenter\Support\RequestDataHelper;

/**
 * 日志记录对象工厂
 * 用于快速构建单条不同类型的业务日志实例对象
 */
class LogFactory
{
    /**
     * 利用提供的数据，构建一个登录日志实例并返回
     *
     * @param  array $values 登录日志数据数组
     * @return LoginLog
     */
    public static function makeLoginLog(array $values)
    {
        if (empty($values['ip'])) {
            $values['ip'] = RequestDataHelper::getWanIp();
        }
        return new LoginLog($values);
    }

    /**
     * 利用提供的数据，构建一个访问日志实例并返回
     *
     * @param  array $values 访问日志数据数组
     * @return AccessLog
     */
    public static function makeAccessLog(array $values)
    {
        if (empty($values['ip'])) {
            $values['ip'] = RequestDataHelper::getWanIp();
        }
        return new AccessLog($values);
    }

    /**
     * 利用提供的数据，构建一个操作日志实例并返回
     *
     * @param  array $values 操作日志数据数组
     * @return OperationLog
     */
    public static function makeOperationLog(array $values)
    {
        if (empty($values['ip'])) {
            $values['ip'] = RequestDataHelper::getWanIp();
        }
        return new OperationLog($values);
    }

    /**
     * 明确构建一个普通的操作日志的日志数据对象。比如正常的添加一个用户数据
     *
     * @param  array  $values 日志数据数组
     * @return OperationLog
     * @throws LogPropsException
     */
    public static function makeCommonOperationLog($values)
    {
        if (empty($values['appCode']) || empty($values['event']) || empty($values['operateItems'])) {
            throw new LogPropsException('无效的日志数据传参');
        }

        foreach ($values['operateItems'] as &$item) {
            if (!empty($item['changeValues'])) {
                throw new LogPropsException('基础操作日志不允许存在变更项内容');
            }
        }

        return new OperationLog($values);
    }

    /**
     * 明确构建一个关联数据变更的操作日志的日志数据对象。比如给用户新增授权一个功能点
     *
     * @param  array  $values 日志数据数组
     * @return OperationLog
     * @throws LogPropsException
     */
    public static function makeRelationOperationLog(array $values)
    {
        if (empty($values['appCode']) || empty($values['event']) || empty($values['operateItems'])) {
            throw new LogPropsException('无效的日志数据传参');
        }

        foreach ($values['operateItems'] as &$item) {
            if (empty($item['changeValues'])) {
                throw new LogPropsException('属性变更操作日志的变更项不能为空');
            }
            if (!is_array($item['changeValues'])) {
                throw new LogPropsException('属性变更项操作日志的变更项必须是数组');
            }

            $item['changeValues'] = static::makeSomeRelationChangeValue($item['changeValues']);
        }

        return new OperationLog($values);
    }

    /**
     * 明确构建一个属性变更的操作日志的日志数据对象。比如用户的手机号发生了变更
     *
     * @param  array  $values 日志数据数组
     * @return OperationLog
     * @throws LogPropsException
     */
    public static function makePropsOperationLog(array $values)
    {
        if (empty($values['appCode']) || empty($values['event']) || empty($values['operateItems'])) {
            throw new LogPropsException('无效的日志数据传参');
        }

        foreach ($values['operateItems'] as &$item) {
            if (empty($item['changeValues'])) {
                throw new LogPropsException('属性变更操作日志的变更项不能为空');
            }
            if (!is_array($item['changeValues'])) {
                throw new LogPropsException('属性变更项操作日志的变更项必须是数组');
            }

            $item['changeValues'] = static::makeSomePropsChangeValue($item['changeValues']);
        }

        return new OperationLog($values);
    }

    /**
     * 根据提供的数据，识别需要构建的设备信息对象类型并返回对应的设备接口实现。
     * 根据 $values 中提供的 type 属性判断设备类型
     *
     * @param  array $values 设备信息数组
     * @return DeviceInterface
     */
    public static function makeDevice(array $values)
    {
        if (strtolower($values['type']) == strtolower(AppDevice::TYPE)) {
            return static::makeAppDevice($values);
        }
        if (strtolower($values['type']) == strtolower(BrowserDevice::TYPE)) {
            return static::makeBrowserDevice($values);
        }
        return new UnknownDevice();
    }

    /**
     * 构建一个移动端 APP 类型的设备信息对象
     *
     * @param  array $values APP 设备信息数据数组
     * @return AppDevice
     */
    public static function makeAppDevice(array $values)
    {
        return new AppDevice($values);
    }

    /**
     * 构建一个浏览器类型的设备信息对象。如果没有提供 userAgent 属性
     * 则会自动尝试从 Request Header 里面去获取，并加载到设备信息中
     *
     * @param  array $values 浏览器设备信息数据数组
     * @return BrowserDevice
     */
    public static function makeBrowserDevice(array $values)
    {
        if (empty($values['userAgent'])) {
            $values['userAgent'] = RequestDataHelper::getUserAgent();
        }
        return new BrowserDevice($values);
    }

    /**
     * 构建一组属性变更项
     *
     * @return PropsChangeValue[]
     */
    protected static function makeSomePropsChangeValue(array $values)
    {
        $result = [];
        foreach ($values as $v) {
            $result[] = new PropsChangeValue($v);
        }
        return $result;
    }

    /**
     * 构建一组关联变更项
     *
     * @return RelationChangeValue[]
     */
    protected static function makeSomeRelationChangeValue(array $values)
    {
        $result = [];
        foreach ($values as $v) {
            $result[] = new RelationChangeValue($v);
        }
        return $result;
    }
}
