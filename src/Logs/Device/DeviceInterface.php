<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月06日11:08:30
 */
namespace GgcpLogCenter\Logs\Device;

interface DeviceInterface
{
    /**
     * 将设备信息格式化成一段字符串
     *
     * @return string
     */
    public function toString();
}