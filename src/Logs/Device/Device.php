<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:34:38
 */
namespace GgcpLogCenter\Logs\Device;

use GgcpLogCenter\Support\Traits\LoadPropsTrait;

/**
 * 设备信息抽象类
 */
abstract class Device implements DeviceInterface
{
    use LoadPropsTrait;
    
    /**
     * 设备类型
     *
     * @var string
     */
    public $type;

    /**
     * 设备名称
     */
    public $name;

    /**
     * 设备运行时操作系统
     *
     * @var string
     */
    public $os;

    public function __construct(array $values = [])
    {
        $this->loadProps($values);
    }

    /**
     * 将设备信息格式化成一段字符串
     *
     * @return string
     */
    abstract public function toString();
}
