<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:39:23
 */
namespace GgcpLogCenter\Logs\Device;

/**
 * App 设备信息类
 */
class AppDevice extends Device
{
    const TYPE = 'App';

    /**
     * App 包版本
     *
     * @var string
     */
    public $appVersion;

    public function __construct(array $values)
    {
        $values['type'] = self::TYPE;
        parent::__construct($values);
    }

    public function toString()
    {
        return sprintf("操作系统: %s  App: %s  版本: %s", $this->os, $this->name, $this->appVersion);
    }
}
