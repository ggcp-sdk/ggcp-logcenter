<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:51:37
 */
namespace GgcpLogCenter\Logs\Device;

use WhichBrowser\Parser;

class BrowserDevice extends Device
{
    const TYPE = 'Browser';

    /**
     * 浏览器内核
     *
     * @var string
     */
    public $engine;

    /**
     * Header 头部数据中的 User-Agent 数据
     *
     * @var string
     */
    public $userAgent;

    public function __construct(array $values)
    {
        $values['type'] = self::TYPE;
        // 自动解析浏览器 UA 信息
        if (!empty($values['userAgent'])) {
            $uaParse      = new Parser($values['userAgent']);
            $this->name   = $uaParse->browser->toString();
            $this->engine = $uaParse->engine->toString();
            $this->os     = $uaParse->os->toString();
        }
        parent::__construct($values);
    }

    public function toString()
    {
        return sprintf("操作系统: %s  浏览器信息: %s", $this->os, $this->name);
    }
}
