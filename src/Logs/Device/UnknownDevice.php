<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月06日11:01:36
 */
namespace GgcpLogCenter\Logs\Device;

/**
 * 未知设备信息类
 */
class UnknownDevice extends Device
{
    public function toString()
    {
        return '未知';
    }
}