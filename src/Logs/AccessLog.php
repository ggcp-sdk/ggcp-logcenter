<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:59:19
 */
namespace GgcpLogCenter\Logs;

use GgcpLogCenter\Exceptions\LogPropsException;

/**
 * 访问日志
 */
class AccessLog extends BaseLog
{
    /**
     * 访问的产品标识
     *
     * @var string
     */
    public $appCode;

    /**
     * 访问的功能路径
     *
     * @var string
     */
    public $accessPath;

    public function __construct(array $values)
    {
        if (empty($values['appCode']) || empty($values['accessPath'])) {
            throw new LogPropsException('访问日志必填信息 appCode,accessPath 缺失');
        }
        parent::__construct($values);
    }
}
