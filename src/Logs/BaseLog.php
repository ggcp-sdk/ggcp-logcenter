<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:25:25
 */
namespace GgcpLogCenter\Logs;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Logs\Device\AppDevice;
use GgcpLogCenter\Logs\Device\BrowserDevice;
use GgcpLogCenter\Logs\Device\DeviceInterface;
use GgcpLogCenter\Logs\Device\UnknownDevice;
use GgcpLogCenter\Support\Traits\ArrayableTrait;
use GgcpLogCenter\Support\Traits\LoadPropsTrait;

abstract class BaseLog implements LogInterface
{
    use ArrayableTrait;
    use LoadPropsTrait;

    /**
     * 触发日志的操作用户 ID
     *
     * @var string
     */
    public $userId;

    /**
     * 操作用户的登录态标识
     *
     * @var string
     */
    public $sessionKey;

    /**
     * 租户号
     *
     * @var string
     */
    public $tenantCode;

    /**
     * 触发日志的系统标识
     *
     * @var string
     */
    public $system;

    /**
     * 产生日志的客户端 IP 地址
     *
     * @var string
     */
    public $ip;

    /**
     * 客户端设备信息
     *
     * @var DeviceInterface
     */
    public $device;

    /**
     * 额外的备注信息
     *
     * @var string
     */
    public $remark;

    /**
     * 记录日志的时间戳（秒）
     *
     * @var int
     */
    public $timestamp;

    public function __construct(array $values = [])
    {
        if (empty($values['userId']) || empty($values['tenantCode']) || empty($values['system'])) {
            throw new LogPropsException('日志必填信息 userId,tenantCode,system 缺失');
        }

        if (!empty($values['device']) && !($values['device'] instanceof DeviceInterface)) {
            $values['device'] = $this->makeDevice($values['device'] ?? []);
        }
        if (empty($values['timestamp'])) {
            $values['timestamp'] = time();
        }
        $this->loadProps($values);
    }

    /**
     * 构建一个设备信息对象实例
     *
     * @param  array $values 设备信息数组
     * @return DeviceInterafce
     */
    protected function makeDevice(array $values)
    {
        if (isset($values['type']) && strtolower($values['type']) == strtolower(AppDevice::TYPE)) {
            return new AppDevice($values);
        }
        if (isset($values['type']) && strtolower($values['type']) == strtolower(BrowserDevice::TYPE)) {
            return new BrowserDevice($values);
        }
        return new UnknownDevice();
    }
}
