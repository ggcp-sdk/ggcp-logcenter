<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日17:02:49
 */
namespace GgcpLogCenter\Logs;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Logs\Operation\OperateItem;

/**
 * 操作日志
 */
class OperationLog extends BaseLog
{
    /**
     * 执行操作行为的产品标识
     *
     * @var string
     */
    public $appCode;

    /**
     * 操作事件
     *
     * @var string
     */
    public $event;

    /**
     * 操作明细详情（操作项集合）
     *
     * @var OperateItem[]
     */
    public $operateItems;

    /**
     * 构造一个操作日志实例对象
     */
    public function __construct(array $values)
    {
        if (empty($values['event']) || empty($values['appCode'])) {
            throw new LogPropsException('操作日志必填信息 appCode,event 缺失');
        }

        $values['operateItems'] = $this->makeOperateItems($values['operateItems'] ?? []);
        parent::__construct($values);
    }

    /**
     * 构建操作日志的操作项明细数据对象
     *
     * @param  array $values 操作项明细数组
     * @return OperateItem[]
     */
    protected function makeOperateItems(array $values)
    {
        $items = [];
        foreach ($values as $v) {
            if ($v instanceof OperateItem) {
                $items[] = $v;
                continue;
            }
            if (!is_array($v)) {
                continue;
            }
            $items[] = new OperateItem($v);
        }
        return $items;
    }
}
