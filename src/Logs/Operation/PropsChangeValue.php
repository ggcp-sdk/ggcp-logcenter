<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月07日18:03:35
 */
namespace GgcpLogCenter\Logs\Operation;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Support\Traits\LoadPropsTrait;

class PropsChangeValue implements ChangeValueInterface
{
    use LoadPropsTrait;

    /**
     * 变更的数据属性名
     *
     * @var string
     */
    public $name;

    /**
     * 变更的属性值内容
     *
     * @var mixed
     */
    public $value;

    /**
     * 变更之前的属性值内容
     *
     * @var mixed
     */
    public $beforeValue;

    public function __construct(array $value = [])
    {
        if (empty($value['name']) || empty($value['value'])) {
            throw new LogPropsException('属性变更项必填信息 name,value 不能为空');
        }
        $this->loadProps($value);
    }
}
