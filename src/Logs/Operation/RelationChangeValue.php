<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日17:14:59
 */
namespace GgcpLogCenter\Logs\Operation;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Support\Traits\LoadPropsTrait;

/**
 * 操作日志 - 操作项 - 发生变更的数据项
 * TODO: 可以探索一下非「新增/移除」的关联数据操作动作时，如何更优雅的区分日志内容
 */
class RelationChangeValue implements ChangeValueInterface
{
    use LoadPropsTrait;

    /** 新增关联动作 */
    const ACTION_ADD = 'add';

    /** 移除关联动作 */
    const ACTION_REMOVE = 'remove';

    /** 无内置关联动作，需要自定义关联数据的动作描述时可以使用 */
    const ACTION_NONE = 'none';

    /**
     * 发生变更的数据项名称
     *
     * @var string
     */
    public $name;

    /**
     * 关联变更的动作类型：添加|移除
     *
     * @var string
     */
    public $action;

    public function __construct(array $value = [])
    {
        if (empty($value['name']) || empty($value['action'])) {
            throw new LogPropsException('操作日志变更数据项必填信息 name,action 不能为空');
        }
        $this->loadProps($value);
    }
}
