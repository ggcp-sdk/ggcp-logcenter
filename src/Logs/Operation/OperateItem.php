<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日17:08:46
 */
namespace GgcpLogCenter\Logs\Operation;

use GgcpLogCenter\Exceptions\LogPropsException;
use GgcpLogCenter\Support\Traits\LoadPropsTrait;
use GgcpLogCenter\Logs\Operation\ChangeValueInterface;

/**
 * 操作日志 - 单个操作项
 */
class OperateItem
{
    use LoadPropsTrait;

    /**
     * 被操作的对象 ID
     *
     * @var string
     */
    public $objectId;

    /**
     * 被操作的对象名称
     *
     * @var string
     */
    public $objectName;

    /**
     * 被操作对象下发生数据变更的数据项内容
     *
     * @var ChangeValueInterface[]
     */
    public $changeValues;

    public function __construct(array $values = [])
    {
        if (empty($values['objectId']) && empty($values['objectName'])) {
            throw new LogPropsException('操作日志操作项必填信息 objectId/objectName 不能同时为空');
        }

        $values['changeValues'] = $this->makeChangeValues($values['changeValues'] ?? []);
        $this->loadProps($values);
    }

    /**
     * @param  array $values 操作日志变更数据项数组
     * @return ChangeValueInterface[]
     */
    protected function makeChangeValues(array $values)
    {
        $items = [];
        foreach ($values as $v) {
            if ($v instanceof ChangeValueInterface) {
                $items[] = $v;
                continue;
            }
            if (!is_array($v)) {
                continue;
            }
            if (isset($v['action'])) {
                // 如果有 action 的数据，则认为是关联类型数据变更
                $items[] = new RelationChangeValue($v);
            } else {
                $items[] = new PropsChangeValue($v);
            }
        }
        return $items;
    }
}
