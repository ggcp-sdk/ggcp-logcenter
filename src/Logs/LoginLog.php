<?php
/**
 * @author huangyj01 <huangyj01@mingyuanyun.com>
 * @since 2022年04月02日16:24:43
 */
namespace GgcpLogCenter\Logs;

use GgcpLogCenter\Exceptions\LogPropsException;

/**
 * 用户登录日志
 */
class LoginLog extends BaseLog
{
    /** 登录动作 */
    const LOGIN = 'LOGIN';
    /** 登出动作 */
    const LOGOUT = 'LOGOUT';

    /**
     * 登录动作类型
     *
     * @var string
     */
    public $action;

    public function __construct(array $values)
    {
        if (empty($values['action'])) {
            throw new LogPropsException('登录日志必填信息 action 缺失');
        }
        parent::__construct($values);
    }
}
