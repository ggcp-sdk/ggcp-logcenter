<?php
/**
 * @author iSakura <huangyj01@mingyuanyun.com>
 */
namespace GgcpLogCenter\Tests;

use GgcpLogCenter\Client;
use GgcpLogCenter\Http\Client as HttpClient;
use GgcpLogCenter\Logs\LogFactory;
use GgcpLogCenter\Logs\LoginLog;
use GgcpLogCenter\Logs\OperationLog;
use GgcpLogCenter\Result;
use PHPUnit\Framework\TestCase;

class HttpClientTest extends TestCase
{
    public function prepareConfig()
    {
        $config = [
            'type' => 'http',
            'http' => [
                'host'                => 'https://www.domain.com',
                'saveLoginLogUri'     => '/save-login-log',
                'saveAccessLogUri'    => '/save-access-log',
                'saveOperationLogUri' => '/save-operation-log',
            ],
        ];
        return $config;
    }

    public function testClient()
    {
        $config = $this->prepareConfig();
        $client = Client::instance($config)->asHttpClient();

        $this->assertInstanceOf(HttpClient::class, $client, 'Client as HttpClient is failure.');

        return $client;
    }

    /**
     * @depends testClient
     */
    public function testSaveLoginLog(HttpClient $client)
    {
        $value = [
            'userId'     => 'xxx-xxx-xxx',
            'tenantCode' => 'testTenant',
            'system'     => 'composerTest',
            'ip'         => '127.0.0.1',
            'device'     => [
                'type'      => 'browser',
                'userAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            ],
            'action'     => LoginLog::LOGIN,
        ];

        $log = LogFactory::makeLoginLog($value);

        $this->assertInstanceOf(LoginLog::class, $log, 'make LoginLog is failure.');

        $res = $client->invoke($log);

        $this->assertInstanceOf(Result::class, $res, 'handle log and get result is failure.');

        if (!$res->ok) {
            $this->assertEmpty($res->errmsg, 'faild, but not error message.');
            echo '记录日志失败:' . $res->errmsg;
        }
    }

    /**
     * @depends testClient
     */
    public function testSaveAccessLog(HttpClient $client)
    {
        $value = [
            'userId'     => 'xxx-xxx-xxx',
            'tenantCode' => 'testTenant',
            'system'     => 'composerTest',
            'ip'         => '127.0.0.1',
            'device'     => [
                'type'      => 'browser',
                'userAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            ],
            'product'    => 'BasicService',
            'accessPath' => '/Composer/mingyuanyun/ggcp-logcenter',
        ];

        $log = LogFactory::makeAccessLog($value);

        $this->assertInstanceOf(AccessLog::class, $log, 'make AccessLog is failure.');

        $res = $client->invoke($log);

        $this->assertInstanceOf(Result::class, $res, 'handle log and get result is failure.');

        if (!$res->ok) {
            $this->assertEmpty($res->errmsg, 'faild, but not error message.');
            echo '记录日志失败:' . $res->errmsg;
        }
    }

    /**
     * @depends testClient
     */
    public function testSaveOperationLog(HttpClient $client)
    {
        $value = [
            'userId'       => 'xxx-xxx-xxx',
            'tenantCode'   => 'test',
            'system'       => 'test',
            'ip'           => '127.0.0.1',
            'device'       => [
                'type'      => 'browser',
                'userAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            ],
            'appCode'      => 'TestAppCode',
            'event'        => 'EVENT',
            'operateItems' => [
                [
                    'objectName'   => '数据A',
                    'changeValues' => [
                        ['name' => '属性a', 'value' => 'a01', 'beforeValue' => 'a02'],
                        ['name' => '属性b', 'value' => 'b01', 'beforeValue' => 'b02'],
                    ],
                ],
                [
                    'objectName'   => '数据B',
                    'changeValues' => [
                        ['name' => '属性a', 'value' => 'a01', 'beforeValue' => 'a02'],
                        ['name' => '属性b', 'value' => 'b01', 'beforeValue' => 'b02'],
                    ],
                ],
            ],
        ];

        $log = LogFactory::makeOperationLog($value);

        $this->assertInstanceOf(OperationLog::class, $log, 'make OperationLog is failure.');

        $res = $client->invoke($log);

        $this->assertInstanceOf(Result::class, $res, 'handle log and get result is failure.');

        if (!$res->ok) {
            $this->assertEmpty($res->errmsg, 'faild, but not error message.');
            echo '记录日志失败:' . $res->errmsg;
        }
    }

    /**
     * @depends testClient
     */
    public function testSaveCommonOperationLog(HttpClient $client)
    {
        $value = [
            'userId'       => 'xxx-xxx-xxx',
            'tenantCode'   => 'test',
            'system'       => 'test',
            'ip'           => '127.0.0.1',
            'device'       => [
                'type'      => 'browser',
                'userAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            ],
            'appCode'      => 'TestAppCode',
            'event'        => 'EVENT',
            'operateItems' => [
                [
                    'objectName' => '数据A',
                ],
                [
                    'objectName' => '数据B',
                ],
            ],
        ];

        $log = LogFactory::makeCommonOperationLog($value);

        $this->assertInstanceOf(OperationLog::class, $log, 'make OperationLog is failure.');

        $res = $client->invoke($log);

        $this->assertInstanceOf(Result::class, $res, 'handle log and get result is failure.');

        if (!$res->ok) {
            $this->assertEmpty($res->errmsg, 'faild, but not error message.');
            echo '记录日志失败:' . $res->errmsg;
        }
    }

    /**
     * @depends testClient
     */
    public function testSaveRelationOperationLog(HttpClient $client)
    {
        $value = [
            'userId'       => 'xxx-xxx-xxx',
            'tenantCode'   => 'test',
            'system'       => 'test',
            'ip'           => '127.0.0.1',
            'device'       => [
                'type'      => 'browser',
                'userAgent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
            ],
            'appCode'      => 'TestAppCode',
            'event'        => 'EVENT',
            'operateItems' => [
                [
                    'objectName'   => '数据A',
                    'changeValues' => [
                        ['action' => 'add', 'name' => '测试功能点01'],
                        ['action' => 'add', 'name' => '测试功能点02'],
                    ],
                ],
                [
                    'objectName'   => '数据B',
                    'changeValues' => [
                        ['action' => 'add', 'name' => '测试功能点01'],
                        ['action' => 'add', 'name' => '测试功能点02'],
                    ],
                ],
            ],
        ];

        $log = LogFactory::makeRelationOperationLog($value);

        $this->assertInstanceOf(OperationLog::class, $log, 'make OperationLog is failure.');

        $res = $client->invoke($log);

        $this->assertInstanceOf(Result::class, $res, 'handle log and get result is failure.');

        if (!$res->ok) {
            $this->assertEmpty($res->errmsg, 'faild, but not error message.');
            echo '记录日志失败:' . $res->errmsg;
        }
    }
}
